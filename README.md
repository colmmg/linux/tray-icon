# tray-icon
Creates a system tray icon with clickable menus. Menu items are configured in the `~/tray.json` config file. See sample JSON config file in root of this repo.

# Prerequisites
Install the `gir1.2-appindicator3` and `yad` packages:
```
sudo apt-get update -y
sudo apt-get install -y gir1.2-appindicator3
sudo apt-get install -y yad
```

Install the `ipinfo` Python library:
```
pip3 install ipinfo
```

Install the `humanzie` Python libray:
```
pip3 install humanize
```

Signup for a free account at [ipinfo.io](https://ipinfo.io/) to obtain an access token which will be set as environment variable `IPINFO_TOKEN`.

# Usage
Run with:
```
export IPINFO_TOKEN="abcd1234" # The value you get from your account with https://ipinfo.io
python3 tray.py
```

# Useful Links
- https://stackoverflow.com/questions/20344674/bash-tool-to-put-an-icon-on-taskbar
- https://fosspost.org/custom-system-tray-icon-indicator-linux/
- https://lazka.github.io/pgi-docs/#AyatanaAppIndicator3-0.1/classes/Indicator.html
- https://askubuntu.com/questions/751608/how-can-i-write-a-dynamically-updated-panel-app-indicator
- https://github.com/ipinfo/python
