#!/usr/bin/python
import os
import subprocess
import json
import ipinfo
import gi
import shutil
import humanize
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
from gi.repository import Gtk as gtk, AppIndicator3 as appindicator
from datetime import datetime
import _thread
from gi.repository import GLib as glib

HOME = os.getenv("HOME")
CONFIG_FILE = f"{HOME}/tray.json"
IPINFO_TOKEN = os.getenv("IPINFO_TOKEN")

ip_timezone_tray = None
ip_city_tray = None
ip_address_tray = None
disk_free_tray = None
local_time_tray = None
utc_time_tray = None

def main():
  indicator = appindicator.Indicator.new("customtray", "semi-starred-symbolic", appindicator.IndicatorCategory.APPLICATION_STATUS)
  indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
  indicator.set_menu(menu())
  gtk.main()

def menu():

  global ip_timezone_tray
  global ip_city_tray
  global ip_address_tray
  global disk_free_tray
  global local_time_tray
  global utc_time_tray

  menu = gtk.Menu()

  with open(CONFIG_FILE) as f:
    CONFIG = json.load(f)

  ip_details = ipinfo.getHandler(IPINFO_TOKEN).getDetails()
  ip_timezone_tray = gtk.MenuItem.new_with_label(f"IP REGION: {ip_details.timezone}")
  ip_city_tray = gtk.MenuItem.new_with_label(f"IP CITY: {ip_details.city}")
  ip_address_tray = gtk.MenuItem.new_with_label(f"IP ADDR: {ip_details.ip}")

  menu.append(ip_timezone_tray)
  menu.append(ip_city_tray)
  menu.append(ip_address_tray)

  menu_sep = gtk.SeparatorMenuItem()
  menu.append(menu_sep)

  disk_total, disk_used, disk_free = shutil.disk_usage("/home")
  disk_free_human = humanize.naturalsize(disk_free)
  local_time = datetime.now().strftime('%H:%M:%S')
  utc_time = datetime.utcnow().strftime('%H:%M:%S')
  disk_free_tray = gtk.MenuItem.new_with_label(f"FREE DISK SPACE: {disk_free_human}")
  local_time_tray = gtk.MenuItem.new_with_label(f"LOC TIME: {local_time}")
  utc_time_tray = gtk.MenuItem.new_with_label(f"UTC TIME: {utc_time}")

  menu.append(disk_free_tray)
  menu.append(local_time_tray)
  menu.append(utc_time_tray)

  menu_sep2 = gtk.SeparatorMenuItem()
  menu.append(menu_sep2)

  for menu_item in CONFIG:
    menu_title = menu_item["title"]
    menu_command = menu_item["command"]
    menu_output = menu_item["output"]
    command = gtk.MenuItem.new_with_label(menu_title)
    command.connect('activate', run_command, menu_command, menu_output)
    menu.append(command)

  exit_tray = gtk.MenuItem.new_with_label('Exit')
  exit_tray.connect('activate', quit)
  menu.append(exit_tray)

  _thread.start_new_thread(update_labels, ())

  menu.show_all()
  return menu

def quit(_):
  gtk.main_quit()

def update_labels():
  value = glib.timeout_add_seconds(1, handler_timeout)

def handler_timeout():
  disk_total, disk_used, disk_free = shutil.disk_usage("/home")
  disk_free_human = humanize.naturalsize(disk_free)
  disk_free_tray.set_label(f"FREE DISK SPACE: {disk_free_human}")
  local_time = datetime.now().strftime('%H:%M:%S')
  local_time_tray.set_label(f"LOC TIME: {local_time}")
  utc_time = datetime.utcnow().strftime('%H:%M:%S')
  utc_time_tray.set_label(f"UTC TIME: {utc_time}")
  local_time_secs = datetime.now().strftime('%S')
  if local_time_secs.endswith("0"):
    ip_details = ipinfo.getHandler(IPINFO_TOKEN).getDetails()
    ip_timezone_tray.set_label(f"IP REGION: {ip_details.timezone}")
    ip_city_tray.set_label(f"IP CITY: {ip_details.city}")
    ip_address_tray.set_label(f"IP ADDR: {ip_details.ip}")
  return True

def run_command(_, menu_command, menu_output):
  result = subprocess.run(menu_command, stdout=subprocess.PIPE)
  if menu_output:
    result_text = result.stdout
    result_string = result_text.decode("utf-8")
    result_string_oneline = result_string.replace("\n", "\\n")
    os.system("yad --html --text '" + result_string_oneline + "' --center")

if __name__ == "__main__":
  if ip_timezone_tray is None and ip_city_tray is None and ip_address_tray is None and disk_free_tray is None and local_time_tray is None and utc_time_tray is None:
    main()
